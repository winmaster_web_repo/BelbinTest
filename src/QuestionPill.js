import React from 'react';

function QuestionPill(props) {
    return (
        <div>
            <h2>
                {props.question}
            </h2>
            <hr /> <br />
            <div>
                {props.answers.map(function (elem) {
                    return (<h4>{elem}</h4>)
                })}
            </div>
        </div>
    );
}

export default QuestionPill;