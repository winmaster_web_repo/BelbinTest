import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button } from 'reactstrap';
import QuestionPill from './QuestionPill.js'

class App extends Component {
  constructor(props){
    super(props)
    this.state={
      answers1: ["1.odp1", "2.odp2", "3.odp3"],
      answers2: ["1.odp1", "2.odp2", "3.odp3"] 
    }
  }

  render() {
    console.log(this.state.answers1);
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Role grupowe - Test Belbina</h1>
        </header>
        <p className="App-intro">
        Na każde zagadnienie przypada 10 punktów. Należy rozdzielić je w sposób, który według ciebie najlepiej opisuje Twoje zachowanie.<br /> Punkty te mogą być rozdzielone pomiędzy kilka zdań, a w przypadkach ekstremalnych możesz rozdzielić je na wszystkie zdania lub przydzielić 10 punktów na jedno zdanie. 
        </p>
        <Button color="primary">primary</Button>{' '}
        <QuestionPill question="Question 1" answers={this.state.answers1} />
        <QuestionPill question="Question 2" answers={this.state.answers1} />
      </div>
    );
  }
}

export default App;
